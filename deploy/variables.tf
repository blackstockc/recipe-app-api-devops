variable "prefix" {
  #recipe app api devops
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "blackstockc@gmail.com"
}
